package com.cts.skilltracker.skill_profile_batch.config;

import com.cts.skilltracker.skill_profile_batch.job.MyCustomProcessor;
import com.cts.skilltracker.skill_profile_batch.job.MyCustomReader;
import com.cts.skilltracker.skill_profile_batch.job.MyCustomWriter;
import com.cts.skilltracker.skill_profile_batch.model.UserSkill;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.batch.item.json.JacksonJsonObjectReader;
import org.springframework.batch.item.json.JsonItemReader;
import org.springframework.batch.item.json.builder.JsonItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.client.RestTemplate;

@Configuration
public class BatchConfiguration {

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    public RestTemplate restTemplate;

    @Value("${userskill.url}")
    public String endpoint;


    @Bean
    public Job createJob() {
        return jobBuilderFactory.get("MyJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener())
                .flow(createStep()).end().build();
    }

    @Bean
    public JobExecutionListener listener()
    {
        return new JobCompletionListener();
    }

    @Bean
    public Step createStep() {
        return stepBuilderFactory.get("MyStep")
                .<UserSkill, UserSkill> chunk(1)
                .reader(jsonItemReader())
                .processor(new MyCustomProcessor())
                .writer(new MyCustomWriter(endpoint, restTemplate))
                .build();
    }

    @Bean
    public JsonItemReader<UserSkill> jsonItemReader() {

        ObjectMapper objectMapper = new ObjectMapper();
        // configure the objectMapper as required
        JacksonJsonObjectReader<UserSkill> jsonObjectReader =
                new JacksonJsonObjectReader<>(UserSkill.class);
        jsonObjectReader.setMapper(objectMapper);

        return new JsonItemReaderBuilder<UserSkill>()
                .jsonObjectReader(jsonObjectReader)
                .resource(new ClassPathResource("userskill.json"))
                .name("userSkillJsonItemReader")
                .build();
    }


}
