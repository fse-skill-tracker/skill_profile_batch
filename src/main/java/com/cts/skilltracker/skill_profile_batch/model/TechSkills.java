package com.cts.skilltracker.skill_profile_batch.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TechSkills {
    @Min(value = 0 , message = "Expertise Level can be between 0-20")
    @Max(value = 20 , message = "Expertise Level can be between 0-20")
    private int htmlJS;

    @Min(value = 0 , message = "Expertise Level can be between 0-20")
    @Max(value = 20 , message = "Expertise Level can be between 0-20")
    private int angular;

    @Min(value = 0 , message = "Expertise Level can be between 0-20")
    @Max(value = 20 , message = "Expertise Level can be between 0-20")
    private int react;

    @Min(value = 0 , message = "Expertise Level can be between 0-20")
    @Max(value = 20 , message = "Expertise Level can be between 0-20")
    private int spring;

    @Min(value = 0 , message = "Expertise Level can be between 0-20")
    @Max(value = 20 , message = "Expertise Level can be between 0-20")
    private int restful;

    @Min(value = 0 , message = "Expertise Level can be between 0-20")
    @Max(value = 20 , message = "Expertise Level can be between 0-20")
    private int hibernate;

    @Min(value = 0 , message = "Expertise Level can be between 0-20")
    @Max(value = 20 , message = "Expertise Level can be between 0-20")
    private int git;

    @Min(value = 0 , message = "Expertise Level can be between 0-20")
    @Max(value = 20 , message = "Expertise Level can be between 0-20")
    private int docker;

    @Min(value = 0 , message = "Expertise Level can be between 0-20")
    @Max(value = 20 , message = "Expertise Level can be between 0-20")
    private int jenkins;

    @Min(value = 0 , message = "Expertise Level can be between 0-20")
    @Max(value = 20 , message = "Expertise Level can be between 0-20")
    private int aws;
}
