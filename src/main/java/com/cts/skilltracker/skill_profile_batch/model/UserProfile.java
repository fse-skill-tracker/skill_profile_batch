package com.cts.skilltracker.skill_profile_batch.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserProfile  {

    private String id;

    @NotNull(message="Associate Id needs to be filled in.")
    @Pattern(regexp="^[Cc][Tt][Ss][0-9]{2,27}$", message="Please enter a valid associate ID.")
    private String associateId;

    @Size(min = 5, max=30, message="Name should be atleast 5 character and maximum 30 character long. ")
    @NotNull(message="Name needs to be filled in.")
    @Pattern(regexp="^[A-Za-z][A-Za-z\\s]{4,29}", message="Please enter a valid associate ID.")
    private String name;

    @Pattern(regexp="^[a-zA-Z0-9_!#$%&'*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$", message="Please enter a valid email")
    @NotNull(message="Please enter email.")
    private String email;

    @Size(min = 10, max=10, message="Please enter 10 digits for mobile number. ")
    @NotNull(message="Mobile number needs to be filled in.")
    @Pattern(regexp="^[1-9][0-9]{9}", message="Please enter 10 digits for mobile number. ")
    private String mobile;


}
