package com.cts.skilltracker.skill_profile_batch.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SoftSkills {
    @Min(value = 0 , message = "Expertise Level can be between 0-20")
    @Max(value = 20 , message = "Expertise Level can be between 0-20")
    private int spoken;

    @Min(value = 0 , message = "Expertise Level can be between 0-20")
    @Max(value = 20 , message = "Expertise Level can be between 0-20")
    private int communication;

    @Min(value = 0 , message = "Expertise Level can be between 0-20")
    @Max(value = 20 , message = "Expertise Level can be between 0-20")
    private int aptitude;
}
