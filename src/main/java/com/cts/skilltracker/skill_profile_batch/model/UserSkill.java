package com.cts.skilltracker.skill_profile_batch.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class UserSkill {


    private UserProfile userProfile;

    private TechSkills techSkills;

    private SoftSkills softSkills;
}
