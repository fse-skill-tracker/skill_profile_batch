package com.cts.skilltracker.skill_profile_batch.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SkillTrackerEvent {

    private String associateId;
    private TechSkills techSkills;
    private SoftSkills softSkills;
}
