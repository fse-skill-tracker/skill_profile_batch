package com.cts.skilltracker.skill_profile_batch.job;

import com.cts.skilltracker.skill_profile_batch.model.UserSkill;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.json.Json;
import java.util.List;

@Component
@ComponentScan
public class MyCustomWriter implements ItemWriter<UserSkill> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyCustomWriter.class);


    private String userSkillEndpoint;


     private RestTemplate restTemplate;

     public  MyCustomWriter(){};

     public MyCustomWriter(String url, RestTemplate restTemplate){
         this.userSkillEndpoint = url;
         this.restTemplate = restTemplate;
     }


    @Override
    public void write(List<? extends UserSkill> list) throws Exception {

        for (UserSkill data : list) {
            System.out.println("MyCustomWriter    : Writing data    : " + data);
            ObjectWriter ow =  new ObjectMapper().writer().withDefaultPrettyPrinter();
            String body = ow.writeValueAsString(data);
           // Json.createObjectBuilder().add("userProfile", data.getUserProfile()).build();
            restTemplate.exchange(userSkillEndpoint, HttpMethod.POST, new HttpEntity<>(body, buildHeaders()), String.class);

        }
        System.out.println("MyCustomWriter    : Writing data    : completed");
    }

    private HttpHeaders buildHeaders(){
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");
        return headers;

    }
}
