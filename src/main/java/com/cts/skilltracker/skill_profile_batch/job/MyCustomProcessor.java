package com.cts.skilltracker.skill_profile_batch.job;

import com.cts.skilltracker.skill_profile_batch.model.UserSkill;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

public class MyCustomProcessor  implements ItemProcessor<UserSkill, UserSkill> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyCustomProcessor.class);
    @Override
    public UserSkill process(UserSkill data) throws Exception {
        LOGGER.debug("MyCustomProcessor : Processing data : "+data);
        return data;
    }

}
