package com.cts.skilltracker.skill_profile_batch;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@EnableBatchProcessing
public class SkillProfileBatchApplication {

	public static void main(String[] args) {

		ConfigurableApplicationContext run = SpringApplication.run(SkillProfileBatchApplication.class, args);
		System.exit(SpringApplication.exit(run));
	}

}
